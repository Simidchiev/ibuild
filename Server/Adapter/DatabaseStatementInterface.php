<?php

namespace Adapter;


interface DatabaseStatementInterface
{
    public function execute(array $params = []);

    public function fetchRow();

    public function fetchAll($fetch_style);

    public function fetchObject($className);

    public function rowCount();
}