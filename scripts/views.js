function showView(viewName) {
    $('main > section').hide();
    $('#' + viewName).show();
}
function showViewSubNav(viewSubName){
    $('#main > section' ).hide();
    showView('viewShop');
    $('#' + viewSubName).show();
}

function showMenuHideLinks() {
    //$('#menu li > a').hide();
    showView('viewHome')
    $('#loggedUser').hide();
     if (!sessionStorage.getItem("authToken")) {
        //Logged in user
        $('#linkHome').show();
        $('#linkLogin').show();
        $('#linkRegister').show();
     } else {
        //No user logged in
        $('#linkHome').show();
        $('#linkShop').show();
        $('#linkCart').show();
        $('#linkLogout').show();
        $('#linkAddNewItem').show();
        $('#linkRemoveItem').show();
     }
     
}

function showHomeView() {
    showView('viewHome')
    listHomeItems()
}

function showLoginView() {
    showView('viewLogin');
    $('#formLogin').trigger('reset');
    $('input[name=username]').focus();
}

function showShopView() {
    showView('viewShop')
}

function showViewAdminAddItem() {
    showView('viewAdminAddItem');
}
function showViewAdminRemoveItem() {
    showView('adminRemoveItem');
    loadAllItems();
}

function showCartView() {
    showView('viewCart')
}

function showRegisterView() {
    showView('viewRegister')
    $('#formRegister').trigger('reset');
}

function showForgotPass(){
    showView('viewForgotPass');
    
}
function showUserAccount(){
    showView('viewUserAccount');
    
}
function mailTo(address){
    let email = $('#email-for-pass').val()
    
    $('#email-for-pass').val('')
}
//sub-nav-shop
function showConstructionHardware(){
    listItems('ConstructionAndHardware');    
}

function showElectrivalAppliancesHeating(){
    listItems('ElectricalAppliancesAndHeating');  
}
function showInteriorDecoration(){
    listItems('InteriorAndDecoration');   
}
function showToolsEquipment(){
    listItems('ToolsAndEquipment');  
}
function showYardGarden(){
    listItems('YardAndGarden');  
}
