function login (event){
    event.preventDefault()
    let password = $('#passwordL').val();
    let userData = {
        apiCall: 'login',
        email: $('#usernameL').val(),
        password: password.trim()
    };
    
    ajaxRequest(userData)
    $('#formLogin').trigger('reset');
}

register = (event) => {
    event.preventDefault()
    let password = $("#passwordR").val();
    let userData = {
        apiCall: 'register',
        email: $("#email").val(),
        password: password.trim(),
        confirm: $("#confirm").val(),
        username: $("#usernameR").val()
    }
    if(userData.password !== userData.confirm) {
            showError("Passwords are not equal! Try again!")
            return;
    }
    ajaxRequest(userData)

    $('#formRegister').trigger('reset');
}

function logoutUser() {
    sessionStorage.clear();
    $('#loggedInUser').text('');
    showMenuHideLinks();
     showView('viewHome');
    //listItems();
    showInfo('Logout successful.');
}

ajaxRequest = (userData) => {
    $.ajax({
        method: 'POST',
        contentType: 'application/json',
        url: "http://localhost/ibuild-master/Server/index.php?",
        data: JSON.stringify(userData)
    }).then(success)
        .catch(handleAjaxError);

    function success(userInfo) {
        userInfo = JSON.parse(userInfo)
        saveAuthInSession(userInfo);
        showMenuHideLinks();
        //listItems();
        showInfo('User registration successful.');
        $('#loggedInUser').text('Welcome, ' + sessionStorage.getItem('username') + '!');
        $('#loggedInUser').show();
    }
}
function saveAuthInSession(userInfo) {
    let username = userInfo.username;
    sessionStorage.setItem('username', username);
    $('#loggedInUser').text('Hello, ' + username + '!');
    $('#loggedInUser').show();
}
function addNewItem(event){
    event.preventDefault();

    let itemData = {
        apiCall: 'addItem',
        category: $("#categoryItem").val(),
        price: $("#priceItem").val(),
        name: $("#itemName").val(),
        description: $("#descriptionItem").val(),
        quantity: $('#quantityItem').val()
    }
    console.log(itemData)
    $('#formAddNewItem').trigger('reset');
    
    ajaxRequest(itemData);
    

}
